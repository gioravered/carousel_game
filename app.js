/**
 * Created by Giora on 15/03/2015.
 */

var carouselGame = {
    occupied_seats: [], // holds the current occupied seats (initialized with random 4 occupied seats
    seats_number: 8,
    occupied_seats_number: 4,
    on_start_state: true,
    on_carousel: false,
    player_seat: 0,
    spin_speed: 32,
    points: 0,
    lives: 3,
    carousel: {},

    // Method to Initiate carousel game
    init: function () {
        carouselGame.setPlayerSeat('start_seat', 0);
        // Push 4 Random Occupied Seats to occupied_seats array
        carouselGame.resetRandomOccupiedSeats();

        //Start the spin
        carouselGame.carousel = TweenMax.to("#carousel", carouselGame.spin_speed, {
            rotation: 360,
            repeat: -1,
            ease: Linear.easeNone
        });
    },

    // Method for the Jump button function
    jump: function () {
        // Get the seats that are currently next to the start and finish points
        var angle = document.getElementById("carousel")._gsTransform.rotation;
        var seat_near_start = parseInt(angle / (360 / carouselGame.seats_number)) + 1;
        var seat_near_finish;
        if (seat_near_start < 5) {
            seat_near_finish = seat_near_start + 4;
        } else {
            seat_near_finish = seat_near_start - 4;
        }

        //Check if player is trying to get on / jump off the carousel
        if (carouselGame.on_carousel) {
            if (carouselGame.on_start_state) {
                carouselGame.jumpOff(seat_near_finish, 'finish_seat');
            } else {
                carouselGame.jumpOff(seat_near_start, 'start_seat');
            }
        } else {
            if (carouselGame.on_start_state) {
                carouselGame.jumpOn(seat_near_start, 'start_seat');
            } else {
                carouselGame.jumpOn(seat_near_finish, 'finish_seat');
            }
        }
    },

    // When trying to jump on the carousel from either the start or finish points
    jumpOn: function (seat_number, from_seat) {
        if (carouselGame.occupied_seats.indexOf(seat_number) == -1) {
            carouselGame.setPlayerSeat("seat"+ seat_number, seat_number);
            carouselGame.setSeatFree(from_seat);
            carouselGame.updateScore(10);
            carouselGame.on_carousel = true;
        } else {
            carouselGame.loseLife();
        }
    },
    // When trying to jump off from the carousel to either the finish or start points.
    jumpOff: function(seat_number, to_seat) {
        if (seat_number == carouselGame.player_seat) {
            carouselGame.setSeatFree("seat" + carouselGame.player_seat);
            carouselGame.setPlayerSeat(to_seat, 0);
            carouselGame.updateScore(50);
            // Increase Speed
            carouselGame.carousel.duration(carouselGame.carousel.duration() / 2);
            carouselGame.on_carousel = false;
            carouselGame.on_start_state = to_seat == "finish_seat" ? false : true;
        } else {
            carouselGame.loseLife();
        }
    },

    resetRandomOccupiedSeats: function() {
        var random_seat;
        for (var i = 0; i < carouselGame.occupied_seats.length; i++) {
            carouselGame.setSeatFree("seat"+ carouselGame.occupied_seats[i]);
        }
        carouselGame.occupied_seats = [];
        while (carouselGame.occupied_seats.length < carouselGame.occupied_seats_number) {
            random_seat = (parseInt(Math.random() * 10) % carouselGame.seats_number) + 1;
            if (carouselGame.occupied_seats.indexOf(random_seat) != -1) {
                continue;
            }
            carouselGame.setSeatOccupied(random_seat);
        }
    },
    // Decrease life or end game if no more lives
    loseLife: function() {
        carouselGame.lives--;
        if (!carouselGame.lives) {
            carouselGame.setLivesLabel(0);
            alert("Game Over, click OK to start again");
            carouselGame.resetGame();
        } else {
            carouselGame.setLivesLabel(carouselGame.lives);
        }
    },

    // Update score by adding "points"
    updateScore: function(points) {
        carouselGame.points += points;
        document.getElementById('score-label').innerHTML = "Score: "+ carouselGame.points;

    },

    // Reset the game to initial state
    resetGame: function() {
        carouselGame.on_start_state = true;
        carouselGame.on_carousel = false;
        carouselGame.carousel.duration(32);
        carouselGame.lives = 3;
        carouselGame.points = 0;
        carouselGame.setLivesLabel(3);
        carouselGame.setScoreLabel(0);

        if (carouselGame.player_seat) {
           carouselGame.setSeatFree("seat" + carouselGame.player_seat);
        }
        carouselGame.setPlayerSeat('start_seat', 0);
        carouselGame.setSeatFree('finish_seat');
        carouselGame.resetRandomOccupiedSeats();
    },

    setScoreLabel: function(points) {
        document.getElementById('score-label').innerHTML = "Score: "+ points;
    },
    setLivesLabel: function(lives) {
        document.getElementById('lives-label').innerHTML = "Lives: "+ lives;
    },
    setSeatOccupied: function(seat_number) {
        carouselGame.occupied_seats.push(seat_number);
        document.getElementById("seat" + seat_number).className = "occupied_seat";
    },
    setPlayerSeat: function(seat_element, seat_number) {
        document.getElementById(seat_element).className = "player_seat";
        carouselGame.player_seat = seat_number;
    },
    setSeatFree: function(seat_element) {
        document.getElementById(seat_element).className = seat_element;
    }
}













